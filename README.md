# React_HW 1
modal-window

# Short description
- Dialog tag is used for modal windows.
- Standard method close() isn't used.
- You can press Esc button to close modals.
- After closing modals will unmount from DOM.

