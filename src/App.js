import React, {Component} from 'react';
import styled from 'styled-components';
import Button from './components/Button';
import Modal from './components/Modal';

const WrapperBtn = styled.div`
width: 50vw;
min-height: 15vh;
margin: 50px auto;
display: flex;
justify-content: center;
/* align-items: flex-start; */
gap: 50px;
`



class App extends Component {
    state = {
      firstModalISOpen: false,
      secondModalIsOpen: false
    }
    
    // спочатку зробив метод async
    // але потім вирішив винести відкриття модалки в окремий метод
    openFirstModal = () => {
      this.setState({firstModalISOpen: true}, this.modalShow);
    }

    modalShow = () => document.querySelector('dialog').showModal();

    closeFirstModal = () => {
      this.setState({firstModalISOpen: false});
    }
    
    confirmPress = () => {
      console.log('File has been deleted');
      this.closeFirstModal()
    }

    openSecondModal = () => {
      this.setState({secondModalISOpen: true}, this.modalShow);
    }

    closeSecondModal = () => {
      this.setState({secondModalISOpen: false});
    }

    render() {
        
        return (
          <>
            <WrapperBtn>
            <Button text='Open first modal' backgroundColor='rgba(255, 0, 0, 0.78)' onClick={this.openFirstModal}></Button>
            <Button text='Open second modal' backgroundColor='rgba(5, 195, 247, 0.5)' onClick={this.openSecondModal}></Button>
            </WrapperBtn>
            {this.state.firstModalISOpen && (<Modal closeButton={true} actions={
              <>
                <Button text='Yes' backgroundColor='rgba(0, 0, 0, 0.2)' onClick={this.confirmPress}></Button>
                <Button text='Cancel' backgroundColor='rgba(0, 0, 0, 0.2)' onClick={this.closeFirstModal}></Button>
              </>
            } closeClick={this.closeFirstModal} backgroundColor='#dd231c' header='Do you want to delete this file?' text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?">
              
            </Modal>)}
            {this.state.secondModalISOpen && (<Modal closeButton={false} actions={
                <Button text='Agree' backgroundColor='rgba(0, 0, 0, 0.2)' onClick={this.closeSecondModal}></Button>
            } closeClick={this.closeSecondModal} backgroundColor='#246dcc' header='You have chosen a blue button' text="You see this window because you pres a button. Press Agree or Esc">
              
            </Modal>)}
          </>
        )
    }
}

export default App;
