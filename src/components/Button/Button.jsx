import React, {Component} from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
color: white;
background: ${props => props.backgroundColor};
width: 100px;
height: 40px;
border: 1px solid #888;
border-radius: 8px;
display: flex;
justify-content: center;
align-items: center;
font-weight: 700;
cursor: pointer;

`



class Button extends Component {
        
    render() {
        const {text, onClick, backgroundColor} = this.props;

        return <StyledButton type="button" backgroundColor={backgroundColor} onClick={onClick}>{text}</StyledButton>
    }
}



export default Button;